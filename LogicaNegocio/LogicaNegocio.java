package LogicaNegocio;

import java.awt.Color;
import java.io.*;
import java.util.List;
import java.util.Vector;
import java.util.zip.*;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class LogicaNegocio {
	private String archivo;
	private String destino;
	private Vector<String> ext;
	
	public LogicaNegocio(String archivo, String destino , Vector<String> ext){
		this.archivo=archivo;
		this.destino=destino;
		this.ext= ext;
		ext.add("java"); //por defecto tiene java, me da igual k lo marquen otra vez
	}
	
	public void Zip2Pdf(){
		try {
			
			ZipInputStream zip = new ZipInputStream(new FileInputStream(archivo));
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(destino));
			document.open();
			
			//Se crea la Cabecera con el nombre del fichero .zip
			Print_Cabecera_Doc(document,archivo.substring(archivo.lastIndexOf("\\")+1, archivo.length()));
			
			//se recorren todos los ficheros y directorios de dentro
			WriteDir(document, archivo, zip,ext);
			
			zip.close();
			document.close();
			
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	
	private void Print_Cabecera_Doc(Document document,String nombre) throws DocumentException{

		Font font_header= FontFactory.getFont("Arial");
		font_header.setColor(new BaseColor(Color.WHITE));
		font_header.setSize(12);
		
		Chunk header = new Chunk("Fichero "+nombre, font_header);
		header.setBackground(new BaseColor(Color.BLACK));
		Paragraph p = new Paragraph(header);
		p.setAlignment(Paragraph.ALIGN_CENTER);
		document.add(p);
		
	}
	
	private void Print_Cabecera_Dir(Document document, String directorio) throws DocumentException{
		Font font_dir=FontFactory.getFont("Arial");
		font_dir.setColor(new BaseColor(Color.WHITE));
		font_dir.setSize(11);
		
		Chunk dir = new Chunk("Contenido del directorio "+directorio.substring(0, directorio.length()-1), font_dir);
		dir.setBackground(new BaseColor(Color.GRAY));	
		
		
		document.add(new Paragraph("\n\n"));
		document.add(dir);
		document.add(new Paragraph(""));

	}
	
	private void Print_Cabecera_File(Document document,String fichero) throws DocumentException{
		
		Font font_dir=FontFactory.getFont("Arial");
		font_dir.setColor(new BaseColor(Color.BLACK));
		font_dir.setSize(10);
		
		Chunk file = new Chunk("Archivo "+fichero, font_dir);
		file.setBackground(new BaseColor(Color.GRAY));	
		
		document.add(file);
		
	}
	
	private void WriteFileInPDF(Document document,ZipInputStream zip) throws IOException, DocumentException{
		BufferedReader in = new BufferedReader(new InputStreamReader(zip));
		Paragraph line = new Paragraph();
		String buffer = null;
		
		Font font_file=FontFactory.getFont("Courier");
		font_file.setSize(9);
		line.setFont(font_file);
		
		while((buffer=in.readLine())!=null){
			if(buffer.compareTo("")==0){       //para que me coja los saltos de linea
				document.add(new Paragraph("\n"));	

			}
			else{
				document.add(new Paragraph(buffer));

			}
		}
		in.close();
	}
	
	private void WriteDir(Document document,String dir,ZipInputStream zip, List<String> ext) throws DocumentException, IOException {
		ZipEntry entry;
		
		entry= zip.getNextEntry();
		while (entry != null) {
			if(entry.isDirectory()){
				Print_Cabecera_Dir(document, entry.getName());
				WriteDir(document, entry.getName(), zip, ext);
			}
			else{
				if(ext.contains(entry.getName().substring(entry.getName().lastIndexOf(".")+1))){
					Print_Cabecera_File(document, entry.getName());
					WriteFileInPDF(document,zip);
					document.add(new Paragraph("\n"));
				}
			}				
		entry= zip.getNextEntry();
		}
	}
}


