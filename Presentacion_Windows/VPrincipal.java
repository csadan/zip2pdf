package Presentacion_Windows;

import java.util.Vector;



import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import LogicaNegocio.LogicaNegocio;

import com.cloudgarden.resource.SWTResourceManager;


public class VPrincipal extends org.eclipse.swt.widgets.Composite {

	private Text TXarchivo;
	private Button BTiniciar;
	private Button BTsalir;
	private Text text1;

	private Display display = Display.getDefault();
	private Shell shell = new Shell(display);
	private Text text5;
	private Text text4;
	private Text TXdestino;
	private Text text3;
	private Text TXextensiones;
	private Text Texto_extensiones;
	private Text text2;
	
	private LogicaNegocio ln=null;
	private String archivo=null;
	private String destino = null;
	private Vector<String> ext = new Vector<String>();

	{
		SWTResourceManager.registerResourceUser(this);
	}

	public VPrincipal(Composite parent, int style) {
		super(parent, style);
		initGUI();
	}
	
	/**
	* Initializes the GUI.
	*/
	private void initGUI() {
		try {
			this.setSize(623, 163);
			this.setBackground(SWTResourceManager.getColor(200, 200, 200));
			FormLayout thisLayout = new FormLayout();
			this.setLayout(thisLayout);
			{
				text5 = new Text(this, SWT.NONE);
				FormData text5LData = new FormData();
				text5LData.left =  new FormAttachment(0, 1000, 351);
				text5LData.top =  new FormAttachment(0, 1000, 95);
				text5LData.width = 75;
				text5LData.height = 14;
				text5.setLayoutData(text5LData);
				text5.setText("(Ej: java,txt,....)");
				text5.setBackground(SWTResourceManager.getColor(200, 200, 200));
			}
			{
				text4 = new Text(this, SWT.NONE);
				FormData text4LData = new FormData();
				text4LData.left =  new FormAttachment(0, 1000, 351);
				text4LData.top =  new FormAttachment(0, 1000, 59);
				text4LData.width = 213;
				text4LData.height = 15;
				text4.setLayoutData(text4LData);
				text4.setText("nombre fichero destino(Ej: ejemplo.pdf)");
				text4.setBackground(SWTResourceManager.getColor(200, 200, 200));
			}
			{
				text3 = new Text(this, SWT.NONE);
				FormData text3LData = new FormData();
				text3LData.left =  new FormAttachment(0, 1000, 351);
				text3LData.top =  new FormAttachment(0, 1000, 23);
				text3LData.width = 266;
				text3LData.height = 15;
				text3.setLayoutData(text3LData);
				text3.setText("ruta completa (Ej: C:\\Users\\Usuario1\\ejemplo.zip)");
				text3.setBackground(SWTResourceManager.getColor(200, 200, 200));
			}
			{
				TXextensiones = new Text(this, SWT.NONE);
				FormData TXextensionesLData = new FormData();
				TXextensionesLData.left =  new FormAttachment(0, 1000, 124);
				TXextensionesLData.top =  new FormAttachment(0, 1000, 89);
				TXextensionesLData.width = 215;
				TXextensionesLData.height = 20;
				TXextensiones.setLayoutData(TXextensionesLData);
			}
			{
				Texto_extensiones = new Text(this, SWT.NONE);
				FormData Texto_extensionesLData = new FormData();
				Texto_extensionesLData.left =  new FormAttachment(0, 1000, 24);
				Texto_extensionesLData.top =  new FormAttachment(0, 1000, 89);
				Texto_extensionesLData.width = 86;
				Texto_extensionesLData.height = 20;
				Texto_extensiones.setLayoutData(Texto_extensionesLData);
				Texto_extensiones.setText("Extensiones");
				Texto_extensiones.setBackground(SWTResourceManager.getColor(200, 200, 200));
				Texto_extensiones.setFont(SWTResourceManager.getFont("Times New Roman", 12, 1, false, false));
			}
			{
				FormData text2LData = new FormData();
				text2LData.left =  new FormAttachment(0, 1000, 94);
				text2LData.top =  new FormAttachment(0, 1000, 58);
				text2LData.width = 245;
				text2LData.height = 16;
				text2 = new Text(this, SWT.NONE);
				text2.setLayoutData(text2LData);
				text2.setText("");
			}
			{
				TXdestino = new Text(this, SWT.NONE);
				TXdestino.setText("Destino:");
				FormData TXdestinoLData = new FormData(64, 25);
				TXdestinoLData.left =  new FormAttachment(0, 1000, 24);
				TXdestinoLData.top =  new FormAttachment(0, 1000, 58);
				TXdestinoLData.width = 64;
				TXdestinoLData.height = 25;
				TXdestino.setLayoutData(TXdestinoLData);
				TXdestino.setText("Destino");
				TXdestino.setSize(64, 25);
				TXdestino.setFont(SWTResourceManager.getFont("Times New Roman", 12, 1, false, false));
				TXdestino.setBackground(SWTResourceManager.getColor(200, 200, 200));
			}

			{
				BTiniciar = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData BTiniciarLData = new FormData();
				BTiniciarLData.left =  new FormAttachment(0, 1000, 83);
				BTiniciarLData.top =  new FormAttachment(0, 1000, 121);
				BTiniciarLData.width = 56;
				BTiniciarLData.height = 25;
				BTiniciar.setLayoutData(BTiniciarLData);
				BTiniciar.setText("Iniciar");
				BTiniciar.addMouseListener(new MouseAdapter() {
					public void mouseDown(MouseEvent evt) {
						BTiniciarMouseDown(evt);
					}
				});
			}
			{
				BTsalir = new Button(this, SWT.PUSH | SWT.CENTER);
				FormData BTsalirLData = new FormData();
				BTsalirLData.left =  new FormAttachment(0, 1000, 239);
				BTsalirLData.top =  new FormAttachment(0, 1000, 121);
				BTsalirLData.width = 60;
				BTsalirLData.height = 25;
				BTsalir.setLayoutData(BTsalirLData);
				BTsalir.setText("Salir");
				BTsalir.addMouseListener(new MouseAdapter() {
					public void mouseDown(MouseEvent evt) {
						BTsalirMouseDown(evt);
					}
				});
			}
			{
				text1 = new Text(this, SWT.NONE);
				FormData text1LData = new FormData();
				text1LData.width = 62;
				text1LData.height = 25;
				text1LData.left =  new FormAttachment(0, 1000, 24);
				text1LData.top =  new FormAttachment(0, 1000, 21);
				text1.setLayoutData(text1LData);
				text1.setText("Archivo:");
				text1.setEditable(false);
				text1.setFont(SWTResourceManager.getFont("Times New Roman", 12, 1, false, false));
				text1.setBackground(SWTResourceManager.getColor(200, 200, 200));
			}
			{
				TXarchivo = new Text(this, SWT.MULTI | SWT.WRAP);
				FormData TXarchivoLData = new FormData();
				TXarchivoLData.left =  new FormAttachment(0, 1000, 94);
				TXarchivoLData.top =  new FormAttachment(0, 1000, 21);
				TXarchivoLData.width = 245;
				TXarchivoLData.height = 17;
				TXarchivo.setLayoutData(TXarchivoLData);
				TXarchivo.setText("");
			}
			this.layout();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Auto-generated main method to display this 
	* org.eclipse.swt.widgets.Composite inside a new Shell.
	*/
	public static void main(String[] args) {
		
		
		
		Display display = Display.getDefault();
		Shell shell = new Shell(display);
		shell.setText("ZipToPDF");
		
		VPrincipal inst = new VPrincipal(shell, SWT.NULL);
		
		Point size = inst.getSize();
		shell.setLayout(new FillLayout());
		shell.layout();
		if(size.x == 0 && size.y == 0) {
			inst.pack();
			shell.pack();
		} else {
			Rectangle shellBounds = shell.computeTrim(0, 0, size.x, size.y);
			shell.setSize(shellBounds.width, shellBounds.height);
		}
		
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
	}
	
	
	private void BTsalirMouseDown(MouseEvent evt) {
		shell.close();
		display.close();
	}
	
	private void BTiniciarMouseDown(MouseEvent evt) {
		if(TXarchivo.getText()!=null){
			this.archivo= TXarchivo.getText();
		}
		else{
			throw new NullPointerException();
		}
		
		if(text2.getText()!=""){
			destino=text2.getText();
		}
		else{
			destino=this.archivo.substring(0,archivo.lastIndexOf("."))+".pdf";
		}
		
		String[] extensiones = TXextensiones.getText().split(",");
		for(int i=0;i<extensiones.length;i++) ext.add(extensiones[i]);
		
		ln = new LogicaNegocio(archivo, destino, ext);
		ln.Zip2Pdf();
		
		display.close();
		shell.close();
		
	}
	

}
