package Presentacion_consola;

import java.util.Vector;

import LogicaNegocio.*;

public class Principal {
	
	public static void main(String[] args) {
	
		
		LogicaNegocio ln=null;
		Vector<String> ext = new Vector<String>();
		String archivo=args[0];
		String destino = null;
		
		int i=0;
				if(args.length>1 && args[1].compareTo("-d")==0){
					destino = args[2];
					i=3;
				}
				else{		
					destino = archivo.substring(0,archivo.lastIndexOf("."))+".pdf";
					i=1;
				}
				
				
				while(i<args.length){
					ext.add(args[i]);
					i++;
				}

		System.out.println("El archivo comprimido es: "+archivo);		
		System.out.println("El fichero pdf destino es: "+destino);
		System.out.println("Las extensiones introducidas son: "+ext);
				
		ln= new LogicaNegocio(archivo, destino, ext);
		ln.Zip2Pdf();
		
	}

}
